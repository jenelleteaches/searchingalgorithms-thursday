cmake_minimum_required(VERSION 3.14)
project(SortingAlgorithms_Thursday C)

set(CMAKE_C_STANDARD 99)

add_executable(SortingAlgorithms_Thursday main.c SelectionSort.c)
add_executable(SelectionSort SelectionSort.c)